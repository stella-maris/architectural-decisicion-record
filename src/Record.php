<?php

/**
 * Copyright Andreas Heigl <andreas@heigl.org>
 *
 * Licenses under the MIT-license. For details see the included file LICENSE.md
 */

declare(strict_types=1);

namespace StellaMaris\ArchitecturalDecision;

use Attribute;

#[Attribute]
#[Record('This idea comes from Dmitri Gooses (@dgoosens on twitter).

The general idea is to document and - most importantly - find again
reasons why certain changes to a code-base were done.')]
#[Record('It was a deliberate decision to not use ADR in the package
name or anywhere else to help people to not confuse this with the
Active Domain Responder pattern.')]
class Record
{
	/**
	 * @param string $decision What was decided and why. There usually isn't a need for when as that
	 *                         should be documented by the VCS
	 */
	public function __construct(private string $decision) {}

	public function decision(): string
	{
		return $this->decision;
	}
}
