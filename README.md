# Architectural Decision Record

An idea from Dmitri Goosens to capture architectural decissions in a format
that can be automatically retrieved, analysed and - most importantly - be found again

## Usage

After installation you can add an Adr-attribute wherever you want within your code
to clearly document an architectural decision that lead to a code-creation or -change

```php
<?php

use StellaMaris\ArchitecturalDecision\Record;

#[Record('
We decided to use Architectural Decision Records to better illustrate
why stuff was done.

There is no need to add a time or date or a name as that should already
be reflected within the VCS.

Record-entries can span multiple lines as they use strings 😁
')]
class Foo
{

}
```

## Installation

This package is best installed via `composer`.

```bash
composer require stella-maris/architectural-decision-record
```

## Tricks


The term `ADR` was deliberately not used in the package as that might lead to
confusion with the `Active Domain Responder` pattern.

If you want to use that instead of `Record` though for the attribute,
feel free to create a class alias like this:

```php
<?php


use StellaMaris\ArchitecturalDecision\Record as ADR;

#[ADR('
We decided to use Architectural Decision Records to better illustrate
why stuff was done.

There is no need to add a time or date or a name as that should already
be reflected within the VCS.

Record-entries can span multiple lines as they use strings 😁
')]
class Foo
{

}
```
